$( document ).ready(function() {
    $(".productDeleteIcon").on("click", function (event) {
        event.preventDefault();
        const productId = $(this).data("id");
        $("#productDeleteButton").data("id", productId);

        $("#productDelete").modal('show');
    });

    $("#productDeleteButton").on("click", function (event) {
        event.preventDefault();
        const productId = $(this).data("id");
        $('#productDelete').modal('hide');
        $("#" + productId).fadeOut("slow", function () {
            //animation complete
        })
    })
});


