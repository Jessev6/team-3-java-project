package com.realdolmen.thomasmore.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("VISA")
public class VisaCard extends PaymentMethod {

    private String cardNumber;
    private String CVC;

    public VisaCard() {
    }

    public VisaCard(String cardNumber, String CVC) {
        this.cardNumber = cardNumber;
        this.CVC = CVC;
    }

    public VisaCard(String firstName, String lastName, String cardNumber, String CVC) {
        super(firstName, lastName);
        this.cardNumber = cardNumber;
        this.CVC = CVC;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCVC() {
        return CVC;
    }

    public void setCVC(String CVC) {
        this.CVC = CVC;
    }
}
