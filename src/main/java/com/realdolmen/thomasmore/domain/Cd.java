package com.realdolmen.thomasmore.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;

@Entity
public class Cd extends Product implements Serializable{
    private String publisher;
    private String artists;
    @Temporal(TemporalType.DATE)
    private Date releaseDate = new Date();
    private String genres;
    private String songs;

    public Cd() {
        super();
    }

    public Cd(String publisher, String artists, Date releaseDate, String genres) {
        this.publisher = publisher;
        this.artists = artists;
        this.releaseDate = releaseDate;
        this.genres = genres;
    }

    public Cd(String code, String name, double price, String description, int stock, String publisher, String artists, Date releaseDate, String genres) {
        super(code, name, price, description,stock);
        this.publisher = publisher;
        this.artists = artists;
        this.releaseDate = releaseDate;
        this.genres = genres;
    }

    public Cd(String code, String name, double price, String description,int stock) {
        super(code, name, price, description,stock);
    }

    @Override
    public String getType() {
        return "cd";
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getArtists() {
        return artists;
    }

    public void setArtists(String artists) {
        this.artists = artists;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getSongs() {
        return songs;
    }

    public void setSongs(String songs) {
        this.songs = songs;
    }
}
