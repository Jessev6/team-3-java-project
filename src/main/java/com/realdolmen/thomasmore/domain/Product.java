package com.realdolmen.thomasmore.domain;



import com.realdolmen.thomasmore.service.ImageService;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Inheritance(strategy = InheritanceType.JOINED)
@Entity
public abstract class Product implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String code;
    private String name;
    private double price;
    private String description;
    private int stock;
    private boolean deleted;


    @OneToMany(mappedBy = "product", cascade = {CascadeType.PERSIST, CascadeType.MERGE  })
    private List<OrderLine> orderLines;

    @OneToMany(mappedBy = "product", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER)
    private List<Review> reviews;

    @OneToOne(cascade = {CascadeType.ALL})
    private Image image;

    public String getImagePath(){
        if(image != null)
            return image.getPath();

        return "";
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    /*public List<Image> getImages() {
            return images;
        }

        public void setImages(List<Image> images) {
            this.images = images;
        }

        public void addImage(Image image){
            this.images.add(image);
        }*/
    public Product() {
        reviews = new ArrayList<>();
    }

    public Product(String code, String name, double price, String description, int stock) {
        reviews = new ArrayList<>();
        this.code = code;
        this.name = name;
        this.price = price;
        this.description = description;
        this.stock = stock;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public void addReview(Review review){
        this.reviews.add(review);
    }

    public double getRating(){
        double sum = 0;

        for(Review review: this.reviews) sum+= review.getRating();

        if (sum != 0){
            return (double)Math.round((sum / this.reviews.size())*100.0)/100.0 ;
        } else {
            return 0;
        }
    }

    public int getRatingInt(){
        int sum = 0;

        for(Review review: this.reviews) sum+= review.getRating();

        if (sum != 0){
            return sum / this.reviews.size() ;
        } else {
            return 0;
        }


    }

    public List<String> getImageNames(){
        List<String> images = new ArrayList<>();
        images.add(this.getImagePath());
        return images;
    }

    public abstract String getType();

    public void deleteImage(Image image){
        ImageService imageService = new ImageService();
        if(imageService.deleteImage(image)){
        }
    }

}
