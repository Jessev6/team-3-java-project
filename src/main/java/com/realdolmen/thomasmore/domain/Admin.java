package com.realdolmen.thomasmore.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Admin")
public class Admin extends User{

    public Admin() {
    }

    public Admin(String email, String firstName, String lastName, String password) {
        super(email, firstName, lastName, password);
    }
}
