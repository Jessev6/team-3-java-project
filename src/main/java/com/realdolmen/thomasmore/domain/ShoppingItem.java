package com.realdolmen.thomasmore.domain;

public class ShoppingItem{
    private Product product;
    private int amount;

    public ShoppingItem(Product product, int amount) {
        this.product = product;
        this.amount = amount;
    }

    public Double getSubTotal(){
        return this.product.getPrice() * this.amount;
    }

    public void add(int amount){
        this.amount += amount;
    }

    public Product getProduct() {
        return product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount){
        this.amount = amount;
    }
}
