package com.realdolmen.thomasmore.domain;

import javax.persistence.*;

@Entity
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Transient
    private String host = "http://localhost:5000/images/";

    @Transient
    private String baseImage = "%d.jpg";

    @OneToOne
    private Product product;

    public String getPath(){
        return host + String.format(baseImage, product.getId());
    }

    public String getSimplePath(){
        return String.format(baseImage, product.getId());
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
