package com.realdolmen.thomasmore.domain;

public enum OrderStatus {
    PROCESSING, ORDERED, IN_TRANSIT, DELIVERED
}
