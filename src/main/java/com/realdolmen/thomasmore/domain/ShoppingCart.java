package com.realdolmen.thomasmore.domain;

import com.realdolmen.thomasmore.security.UserSession;
import com.realdolmen.thomasmore.service.OrderLineService;
import com.realdolmen.thomasmore.service.OrderService;
import com.realdolmen.thomasmore.service.ProductService;
import javassist.NotFoundException;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;

import javax.faces.bean.ManagedProperty;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

public class ShoppingCart{

    @ManagedProperty("#{orderService}")
    private OrderService orderService;

    @ManagedProperty("#{orderLineService}")
    private OrderLineService orderLineService;

    @ManagedProperty("#{userSession}")
    private UserSession userSession;

    private ArrayList<ShoppingItem> items = new ArrayList<>();
    private int itemCount;
    private double totalPrice;

    public ShoppingCart() {
    }

    public ArrayList<ShoppingItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<ShoppingItem> items) {
        this.items = items;
    }

    //Het totaal aantal producten in het winkelmandje weergeven
    public int getItemCount(){
        return items.stream().mapToInt(ShoppingItem::getAmount).sum();
    }

    //Totale prijs van alle producten in winkelmandje opvragen
    public double getTotalPrice(){
        double totalPrice = 0.0;
        for (ShoppingItem item: items) {
            totalPrice += item.getProduct().getPrice() * item.getAmount();
        }
        return totalPrice;
    }

    //Product toevoegen met aantal en controle of het product al bestaat
    public void addProduct(Product product, int amount){
        if (items.stream().anyMatch(item -> item.getProduct() == product)){
            items.stream().filter(item -> item.getProduct() == product).findFirst().get().add(amount);
        } else {
            items.add(new ShoppingItem(product, amount));
        }
    }

    //Product verwijderen
    public void removeProduct(Product product){
        for(Iterator<ShoppingItem> iter = items.iterator(); iter.hasNext();){
            if(iter.next().getProduct() == product){
                iter.remove();
                break;
            }
        }
    }

    //Product aantal aanpassen
    //Geeft NotFoundException als het product niet in de shopping cart zit
    public void changeProductAmount(Product product, int amount) throws NotFoundException{
        if (items.stream().anyMatch(item -> item.getProduct() == product)){
            items.stream().filter(item -> item.getProduct() == product).findFirst().get().setAmount(amount);
        } else {
            throw new NotFoundException("The specified product was not found in the shopping cart");
        }
    }

    //INDIEN NODIG
    //Voorzien voor het winkelmandje in de database bij te houden
        public String toShortString(){
        StringBuilder sb = new StringBuilder();
        for (ShoppingItem item: items) {
            sb.append('$');
            sb.append(item.getProduct().getId());
            sb.append(':');
            sb.append(item.getAmount());
            sb.append(',');
        }
        return sb.toString();
    }

    //INDIEN NODIG
    //Winkelkarretje opvullen met gegevens uit database
    public void parseFromShortString(String cart){
        ProductService productService = new ProductService();
        String[] cartItems = cart.split(",");
        for (String item: cartItems) {
            String[] details = item.split(":");
            Product product = productService.findById(Integer.parseInt(details[0]));
            int amount = Integer.parseInt(details[1]);
            this.addProduct(product, amount);
        }
    }

    //INDIEN NODIG
    //Om makkelijk met het winkelmandje te werken in javascript
    public JSONObject toJSON(){
        JSONObject json = new JSONObject();
        JSONArray jsonItems = new JSONArray();
        for (ShoppingItem item : items) {
            JSONObject i = new JSONObject();
            i.put("product_id", item.getProduct().getId());
            i.put("amount", new Integer(item.getAmount()));
            jsonItems.put(i);
        }
        json.put("cart", jsonItems);
        json.put("total_price", this.totalPrice);
        json.put("item_count", this.itemCount);
        return json;
    }

    public void makeOrder(){
        orderService.saveOrder(this.toOrder());
    }

    public Order toOrder(){
        Order order = new Order();
        for(ShoppingItem item : items){
            order.getOrderLines().add(new OrderLine(item.getProduct().getPrice(), itemCount, item.getProduct(), order));
        }
        order.setOrderDate(new Date());
        order.setCustomer((Customer) userSession.getUser());
        order.setOrderStatus(OrderStatus.ORDERED);
        order.setPaymentMethod(new VisaCard("Arne", "Van Bael", "4111111111111111", "111"));
        order.setDeliveryDate(new Date());
        return order;
    }

    public void setUserSession(UserSession userSession) {
        this.userSession = userSession;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public void setOrderLineService(OrderLineService orderLineService) {
        this.orderLineService = orderLineService;
    }
}
