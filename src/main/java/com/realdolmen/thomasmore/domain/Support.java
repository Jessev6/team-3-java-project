package com.realdolmen.thomasmore.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@DiscriminatorValue("Support")
public class Support extends User {

    public Support() {
    }

    public Support(String email, String firstName, String lastName, String password) {
        super(email, firstName, lastName, password);
    }

    @OneToMany(mappedBy = "support")
    private List<Ticket> tickets = new ArrayList<Ticket>();

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }
}
