package com.realdolmen.thomasmore.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@DiscriminatorValue("Customer")
public class Customer extends User {

    private boolean active;

    @Embedded
    private Address billingAddress;
    @AttributeOverrides({
            @AttributeOverride(name = "street", column = @Column(name = "billingStreet")),
            @AttributeOverride(name = "postalCode", column = @Column(name = "billingPostalCode")),
            @AttributeOverride(name = "city", column = @Column(name = "billingCity"))
    })

    @Embedded
    private Address deliveryAddress1;
    @AttributeOverrides({
            @AttributeOverride(name = "street", column = @Column(name = "deliveryStreet1")),
            @AttributeOverride(name = "postalCode", column = @Column(name = "deliveryPostalCode1")),
            @AttributeOverride(name = "city", column = @Column(name = "deliveryCity1"))
    })

    @Embedded
    private  Address deliveryAddress2;
    @AttributeOverrides({
            @AttributeOverride(name = "street", column = @Column(name = "deliveryStreet2")),
            @AttributeOverride(name = "postalCode", column = @Column(name = "deliveryPostalCode2")),
            @AttributeOverride(name = "city", column = @Column(name = "deliveryCity2"))
    })

    @OneToMany(mappedBy = "customer", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<Order> orders;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinTable(name="CUSTOMER_PAYMENTMETHOD")
    private List<PaymentMethod> paymentMethods;

    public Customer() {
    }

    public Customer(String email, String firstName, String lastName, String password) {
        super(email, firstName, lastName, password);
    }

    public Customer(String email, String firstName, String lastName, Address billingAddress, Address deliveryAddress1, Address deliveryAddress2, String password) {
        super(email, firstName, lastName, password);
        this.billingAddress = billingAddress;
        this.deliveryAddress1 = deliveryAddress1;
        this.deliveryAddress2 = deliveryAddress2;
    }

    public Address getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(Address billingAddress) {
        this.billingAddress = billingAddress;
    }

    public Address getDeliveryAddress1() {
        return deliveryAddress1;
    }

    public void setDeliveryAddress1(Address deliveryAddress1) {
        this.deliveryAddress1 = deliveryAddress1;
    }

    public Address getDeliveryAddress2() {
        return deliveryAddress2;
    }

    public void setDeliveryAddress2(Address deliveryAddress2) {
        this.deliveryAddress2 = deliveryAddress2;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public List<PaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(List<PaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
