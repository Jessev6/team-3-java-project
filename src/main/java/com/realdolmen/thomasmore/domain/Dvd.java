package com.realdolmen.thomasmore.domain;


import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Dvd extends Product implements Serializable{
    private String director;
    private String distributor;
    @Temporal(TemporalType.DATE)
    private Date releaseDate = new Date();
    private String imdbLink;
    private String actors;
    private int duration;
    private String languages;
    private String subtitles;
    private String genres;

    public Dvd() {
        super();
    }

    public Dvd(String director, String distributor, Date releaseDate, String imdbLink, String actors, int duration, String languages, String subtitles, String genres){
        this.director = director;
        this.distributor = distributor;
        this.releaseDate = releaseDate;
        this.imdbLink = imdbLink;
        this.actors = actors;
        this.duration = duration;
        this.languages = languages;
        this.subtitles = subtitles;
        this.genres = genres;
    }

    public Dvd(String code, String name, double price, String description,int stock,String director, String distributor, Date releaseDate, String imdbLink, String actors, int duration, String languages, String subtitles, String genres){
        super(code, name, price, description,stock );
        this.director = director;
        this.distributor = distributor;
        this.releaseDate = releaseDate;
        this.imdbLink = imdbLink;
        this.actors = actors;
        this.duration = duration;
        this.languages = languages;
        this.subtitles = subtitles;
        this.genres = genres;
    }

    public Dvd(String code, String name, double price, String description, int stock) {
        super(code, name, price, description, stock);
    }

    @Override
    public String getType() {
        return "dvd";
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getDistributor() {
        return distributor;
    }

    public void setDistributor(String distributor) {
        this.distributor = distributor;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getImdbLink() {
        return imdbLink;
    }

    public void setImdbLink(String imdbLink) {
        this.imdbLink = imdbLink;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }

    public String getSubtitles() {
        return subtitles;
    }

    public void setSubtitles(String subtitles) {
        this.subtitles = subtitles;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }


}
