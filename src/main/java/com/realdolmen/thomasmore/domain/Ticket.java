package com.realdolmen.thomasmore.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.GregorianCalendar;

@Entity
public class Ticket implements Serializable {

    public enum Status {
        NEW,OPEN,PENDING,ON_HOLD,SOLVED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String description;
    private GregorianCalendar date;
    @Enumerated(EnumType.ORDINAL)
    private Status status;
    @ManyToOne
    private Order order;
    @ManyToOne
    private Support support;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GregorianCalendar getDate() {
        return date;
    }

    public void setDate(GregorianCalendar date) {
        this.date = date;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Support getSupport() {
        return support;
    }

    public void setSupport(Support support) {
        this.support = support;
    }

    public Ticket(String description, GregorianCalendar date, Status status, Order order) {
        this.description = description;
        this.date = date;
        this.status = status;
        this.order = order;
    }

    public Ticket() {
    }
}
