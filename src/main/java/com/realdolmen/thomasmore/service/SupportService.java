package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.Support;
import com.realdolmen.thomasmore.repository.SupportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SupportService {
    @Autowired
    private SupportRepository supportRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<Support> findAllSupportEmployees() {
        return supportRepository.findAll();
    }

    public void saveSupport(Support support){
        if(support != null){
            if(support.getId() == 0 ){
                support.setPassword(passwordEncoder.encode(support.getPassword()));
            }else {
                support.setPassword(support.getPassword().trim());
                if(support.getPassword().length() > 0 && !passwordEncoder.matches(support.getPassword(), supportRepository.findOne(support.getId()).getPassword())){
                    support.setPassword(passwordEncoder.encode(support.getPassword()));
                }else {
                    support.setPassword(supportRepository.findOne(support.getId()).getPassword());
                }
            }
            supportRepository.save(support);
        }
    }

    public void deleteSupport(Support support){
        if(support != null){
            this.supportRepository.delete(support);
        }
    }

    public Support findByEmail(String email){return supportRepository.findByEmail(email); }
}