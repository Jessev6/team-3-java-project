package com.realdolmen.thomasmore.service;


import com.realdolmen.thomasmore.domain.Customer;
import com.realdolmen.thomasmore.domain.Order;
import com.realdolmen.thomasmore.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    public void saveOrder(Order order){
        if(order !=null){
            orderRepository.save(order);
        }
    }

    public List<Order> getOrdersByCustomersEmail(String email){
       if(email.trim().length() == 0) return Collections.emptyList();

       return orderRepository.findAllByCustomerEmail(email);
    }

    public void deleteOrder(Order order){
        if(order !=null){
            orderRepository.delete(order);
        }
    }


    public void deleteAll(){
        this.orderRepository.deleteAll();
    }
    public Order get(Long id){ return orderRepository.findById(id); }

    public List<Order> findAllOrders(){ return orderRepository.findAll(); }

    public List<Order> findAllFutureOrdersByCustomer(Customer customer){
        Date date = new Date();
        return orderRepository.findAllByCustomerAndDeliveryDateIsGreaterThanEqual(customer, date);
    }

    public List<Order> findAllHistoryOrdersByCustomer(Customer customer){
        Date date = new Date();
        return orderRepository.findAllByCustomerAndDeliveryDateIsLessThan(customer, date);
    }


}
