package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.Product;
import com.realdolmen.thomasmore.domain.Review;
import com.realdolmen.thomasmore.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.realdolmen.thomasmore.repository.ReviewRepository;
import java.util.Date;
import java.util.List;

@Service
public class ReviewService {
    @Autowired
    private ReviewRepository reviewRepository;

    public void saveReview(Review review){
        if(review !=null){
            reviewRepository.save(review);
        }
    }

    public List<Review> findAllReviews(){
        return reviewRepository.findAll();
    }

    public List<Review> findAllByProduct(Product product){
        return reviewRepository.findAllByProduct(product);
    }

    public List<Review> findAllByProductAndRating(Product product, int rating){
        return reviewRepository.findAllByProductAndRating(product,rating);
    }

    public int countAllByProduct(Product product){
        return reviewRepository.countAllByProduct(product);
    }

    public int countAllByProductAndRating(Product product, int rating){
        return reviewRepository.countAllByProductAndRating(product,rating);
    }

    public Review findByProductAndUser(Product product, User user){
        return  reviewRepository.findByProductAndUser(product, user);
    }
}

