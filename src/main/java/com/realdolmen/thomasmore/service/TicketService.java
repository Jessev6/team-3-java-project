package com.realdolmen.thomasmore.service;


import com.realdolmen.thomasmore.domain.Support;
import com.realdolmen.thomasmore.domain.Ticket;
import com.realdolmen.thomasmore.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketService {

    @Autowired
    private TicketRepository ticketRepository;

    public void saveTicket(Ticket ticket) {
        if (ticket != null) {
            ticketRepository.save(ticket);
        }
    }

    public Ticket get(Long id) {
        return ticketRepository.findById(id);
    }

    public List<Ticket> findAll() {
        return ticketRepository.findAll();
    }

    public List<Ticket> findBySupportAndStatus(Support support, Ticket.Status status) {
        return ticketRepository.findBySupportAndStatus(support, status);
    }

    public List<Ticket> findByStatus(Ticket.Status status) {
        return ticketRepository.findByStatus(status);
    }
}
