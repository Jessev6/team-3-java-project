package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.Product;
import com.realdolmen.thomasmore.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public void saveProduct(Product product) {
        if (product != null) {
            productRepository.save(product);
        }
    }

    public Product findById(long id) {
        return productRepository.findById(id);
    }

    public Product findProductById(Long id) {
        return productRepository.findById(id);
    }

    public List<Product> findAllProducts() {
        return productRepository.findAll();
    }

    public List<Product> findAllByOrderByPriceAsc() {
        return productRepository.findAllByOrderByPriceAsc();
    }

    public List<Product> findAllByOrderByNameAsc() {
        return productRepository.findAllByOrderByNameAsc();
    }
    public List<Product> findByStock(int aantal)
    {
        return productRepository.findAllByStockGreaterThanOrderByStockAsc(aantal);
    }

    public void deleteProduct(Product product) {
        if (product != null) {
            this.productRepository.delete(product);
        }
    }


}