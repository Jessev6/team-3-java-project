package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.Image;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;

@Service
public class ImageService {

    public boolean uploadImage(byte[] data, Image image){
        try{
            HttpClient client = HttpClients.createDefault();
            HttpPost uploadImage = new HttpPost("http://localhost:5000/images/upload");
            File file = new File("temp");
            new FileOutputStream(file).write(data);
            FileBody fb = new FileBody(file);
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.addTextBody("path", image.getSimplePath());
            builder.addPart("image", fb);
            HttpEntity multipart = builder.build();

            uploadImage.setEntity(multipart);
            HttpResponse response = client.execute(uploadImage);
            int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode == 200)
                return true;
        } catch (Exception ex){
            return false;
        }
        return false;
    }

    public boolean deleteImage(Image image){
        return false;
    }

}
