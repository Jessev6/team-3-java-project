package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.User;
import com.realdolmen.thomasmore.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public boolean checkIfUserEmailExists(User user){
        if(userRepository.findByEmail(user.getEmail().trim()) != null){
            //email already in use
            return true;
        }else{
            //email not in use
            return false;
        }
    }

    public  User findByEmail(String email){
        return  userRepository.findByEmail(email);
    }

    public User findById(long id){
        return userRepository.findById(id);
    }
}
