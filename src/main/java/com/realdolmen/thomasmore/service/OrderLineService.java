package com.realdolmen.thomasmore.service;


import com.realdolmen.thomasmore.domain.Order;
import com.realdolmen.thomasmore.domain.OrderLine;
import com.realdolmen.thomasmore.repository.OrderLineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class OrderLineService {

    @Autowired
    private OrderLineRepository orderLineRepository;

    public void saveOrderLine(OrderLine orderLine){
        if(orderLine !=null){
            orderLineRepository.save(orderLine);
        }
    }

    public OrderLine findById(long id) {
        return orderLineRepository.findById(id);
    }

    public void deleteOrderLineById(long id){
        orderLineRepository.delete(id);
    }

    public void deleteOrderLine(OrderLine orderLine){
        orderLineRepository.delete(orderLine);
    }

    public void deleteAll(){
        orderLineRepository.deleteAll();
    }

    public OrderLine get(Long id){ return orderLineRepository.findById(id); }

    public List<OrderLine> findOrderLinesByOrders(Order order){return orderLineRepository.findByOrder(order);}

    public List<OrderLine> findAllOrderLines(){ return orderLineRepository.findAll(); }




}
