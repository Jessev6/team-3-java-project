package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.PaymentMethod;
import com.realdolmen.thomasmore.domain.VisaCard;
import com.realdolmen.thomasmore.repository.VisaCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VisaCardService {
    @Autowired
    private VisaCardRepository visaCardRepository;

    public void saveVisaCard(VisaCard visaCard){
        if(visaCard != null){
            visaCardRepository.save(visaCard);
        }
    }

    public VisaCard get(Long id){
        return visaCardRepository.findOne(id);
    }

    public List<VisaCard> findAll(){
        return visaCardRepository.findAll();
    }

    public void deleteCard(VisaCard card){
        if(card != null) visaCardRepository.delete(card);
    }
}
