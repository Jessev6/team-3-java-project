package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.Address;
import com.realdolmen.thomasmore.domain.Customer;
import com.realdolmen.thomasmore.domain.User;
import com.realdolmen.thomasmore.repository.CustomerRepository;
import com.realdolmen.thomasmore.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<Customer> findAllCustomers() {
        return customerRepository.findAll();
    }

    public long saveCustomer(Customer customer){

        if(customer != null){
            if(customer.getId() == 0){
                customer.setPassword(passwordEncoder.encode(customer.getPassword()));
            }
            customerRepository.save(customer);

        }
        return customer.getId();
    }

    public void updatePassword(Customer customer){
        customer.setPassword(passwordEncoder.encode(customer.getPassword()));
        customerRepository.save(customer);
    }

    public Customer findById(long id){ return customerRepository.findById(id); }

    public Customer findByEmail(String email){return customerRepository.findByEmail(email); }


}
