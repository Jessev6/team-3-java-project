package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.Admin;
import com.realdolmen.thomasmore.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {
    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<Admin> findAllAdmins() {
        return adminRepository.findAll();
    }

    public void saveAdmin(Admin admin){
        if(admin != null ){
            if (admin.getId() == 0){//als het een nieuwe admin is
                admin.setPassword("test");
                admin.setPassword(passwordEncoder.encode(admin.getPassword()));
            }
            else {
                admin.setPassword(admin.getPassword().trim());
                if(admin.getPassword().length() > 0 && !passwordEncoder.matches(admin.getPassword(), adminRepository.findOne(admin.getId()).getPassword())){
                    admin.setPassword(passwordEncoder.encode(admin.getPassword()));
                }else {
                    admin.setPassword(adminRepository.findOne(admin.getId()).getPassword());
                }
            }
            adminRepository.save(admin);
        }
    }

    public void deleteAdmin(Admin admin){
        if(admin != null){
            adminRepository.delete(admin);
        }
    }
}
