package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.Dvd;
import com.realdolmen.thomasmore.repository.DvdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DvdService {

    @Autowired
    private DvdRepository dvdRepository;

    public List<Dvd> findAlldvd(){
        return dvdRepository.findAll();
    }

    public void saveDvd(Dvd dvd){
        if(dvd != null){
            dvdRepository.save(dvd);
        }
    }

    public void deletedvd(Dvd dvd){
        if(dvd != null){
            dvd.setDeleted(true);
            dvdRepository.save(dvd);
        }
    }
}
