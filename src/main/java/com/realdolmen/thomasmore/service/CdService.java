package com.realdolmen.thomasmore.service;

import com.realdolmen.thomasmore.domain.Cd;
import com.realdolmen.thomasmore.repository.CdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CdService {

    @Autowired
    private CdRepository cdRepository;

    public List<Cd> findAllCd(){
        return cdRepository.findAllByOrderByNameAsc();
    }

    public void saveCd(Cd cd){
        if(cd != null){
            cdRepository.save(cd);
        }
    }

    public void deleteCd(Cd cd){
        if(cd != null){
            cd.setDeleted(true);
            cdRepository.save(cd);
        }
    }
}
