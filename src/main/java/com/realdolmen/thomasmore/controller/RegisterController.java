package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Customer;
import com.realdolmen.thomasmore.service.CustomerService;
import com.realdolmen.thomasmore.service.EncryptionService;
import com.realdolmen.thomasmore.service.MailService;
import com.realdolmen.thomasmore.service.UserService;


import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class RegisterController {

    @ManagedProperty("#{customerService}")
    private CustomerService customerService;

    @ManagedProperty("#{encryptionService}")
    private EncryptionService encryptionService;

    @ManagedProperty("#{userService}")
    private UserService userService;

    @ManagedProperty("#{mailService}")
    private MailService mailService;

    private String newUserFirstName;
    private String newUserLastName;
    private String newUserEmail;
    private String newUserPassword1;
    private String newUserPassword2;
    private String userId;
    private String message;

    public RegisterController(){
    }

    public String createCustomer() throws Exception{
            Customer customer = new Customer();
            customer.setFirstName(newUserFirstName);
            customer.setLastName(newUserLastName);
            customer.setEmail(newUserEmail.trim());
            customer.setPassword(newUserPassword1);
            customer.setActive(false);
            if(!this.userService.checkIfUserEmailExists(customer)){
                long id = customerService.saveCustomer(customer);
                String encrypted = encryptionService.encrypt(id + "");
                mailService.sendMail(
                        newUserEmail,
                        "Account activeren",
                        "<h1>Welkom</h1><p>U bent er bijna.</p><p>klik <a href=http://localhost:8080/thomas-more-starter-1.0-SNAPSHOT/activateaccount.xhtml?token=" + encrypted + ">hier</a> om uw account te activeren</p>"
                );
                setMessage("Uw account is aangemaakt en een mail is verstuurd waarmee u uw account kan activeren.");
                return "registerconfirmationpage.xhtml";
            }else{
                setMessage("Er is al een account met het opgegeven email adres");
                return "register.xhtml";
            }
    }

    public String activateAccount() throws Exception{
        String encrypted = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("token");
        long id = Long.parseLong(encryptionService.decrypt(encrypted));
        Customer customer = customerService.findById(id);
        customer.setActive(true);
        customerService.saveCustomer(customer);
        setMessage("Uw account is geactiveerd");
        return "registerconfirmationpage.xhtml";
    }

    public CustomerService getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    public String getNewUserFirstName() {
        return newUserFirstName;
    }

    public void setNewUserFirstName(String newUserFirstName) {
        this.newUserFirstName = newUserFirstName;
    }

    public String getNewUserLastName() {
        return newUserLastName;
    }

    public void setNewUserLastName(String newUserLastName) {
        this.newUserLastName = newUserLastName;
    }

    public String getNewUserEmail() {
        return newUserEmail;
    }

    public void setNewUserEmail(String newUserEmail) {
        this.newUserEmail = newUserEmail;
    }

    public String getNewUserPassword1() {
        return newUserPassword1;
    }

    public void setNewUserPassword1(String newUserPassword1) {
        this.newUserPassword1 = newUserPassword1;
    }

    public String getNewUserPassword2() {
        return newUserPassword2;
    }

    public void setNewUserPassword2(String newUserPasword2) {
        this.newUserPassword2 = newUserPasword2;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public EncryptionService getEncryptionService() {
        return encryptionService;
    }

    public void setEncryptionService(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    public MailService getMailService() {
        return mailService;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
