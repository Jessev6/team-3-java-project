package com.realdolmen.thomasmore.controller;


import com.realdolmen.thomasmore.domain.*;
import com.realdolmen.thomasmore.security.UserSession;
import com.realdolmen.thomasmore.service.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;


@ManagedBean
@SessionScoped
public class TicketController {

    @ManagedProperty("#{customerService}")
    private CustomerService customerService;

    @ManagedProperty("#{supportService}")
    private SupportService supportService;

    @ManagedProperty("#{encryptionService}")
    private EncryptionService encryptionService;

    @ManagedProperty("#{userService}")
    private UserService userService;

    @ManagedProperty("#{mailService}")
    private MailService mailService;

    @ManagedProperty("#{orderService}")
    private OrderService orderService;

    @ManagedProperty("#{ticketService}")
    private TicketService ticketService;

    private List<Order> orders = new ArrayList<>();
    private Order order;
    private String orderId;
    private String message;
    private Ticket ticketDetails;
    private String description;
    private String answer;
    private String subject;
    private String status;

    private List<Ticket> newTickets = new ArrayList<>();
    private List<Ticket> openTickets = new ArrayList<>();
    private List<Ticket> onholdTickets = new ArrayList<>();
    private List<Ticket> solvedTickets = new ArrayList<>();


    public Customer getLoggedinCustomer() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = ((UserDetails) principal).getUsername();
        Customer customer = customerService.findByEmail(email);
        return customer;
    }

    public Support getLoggedinSupport() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = ((UserDetails) principal).getUsername();
        Support support = supportService.findByEmail(email);
        return support;
    }

    public void getOrdersByCustomer(Customer customer) {
        orders = orderService.getOrdersByCustomersEmail(customer.getEmail());
    }

    public String customerTicket() {
        Customer customer = getLoggedinCustomer();
        getOrdersByCustomer(customer);
        return "/customer/ticket.xhtml?faces-redirect=true";
    }

    public String saveTicket() {
        Customer customer = getLoggedinCustomer();
        Ticket ticket = new Ticket();
        order = orderService.get(Long.parseLong(orderId));
        if (order.getCustomer().getId() == customer.getId()) {
            ticket.setOrder(order);
            ticket.setDescription(description);
            ticket.setDate(new GregorianCalendar());
            ticket.setStatus(Ticket.Status.NEW);
            ticketService.saveTicket(ticket);
            setMessage("Uw ticket is ingestuurd en we zullen hier zo spoedig mogelijk op antwoorden.");
            return "ticketconfirmation.xhtml?faces-redirect=true";
        } else {
            setMessage("Er is mogelijk iets mis gegaan tijdens het versturen van uw ticket, gelieve dit opnieuw te proberen.");
            return "ticketconfirmation.xhtml?faces-redirect=true";
        }
    }

    public String supportTicketLists() {
        Support support = getLoggedinSupport();
        newTickets = ticketService.findByStatus(Ticket.Status.NEW);
        openTickets = ticketService.findBySupportAndStatus(support, Ticket.Status.OPEN);
        onholdTickets = ticketService.findBySupportAndStatus(support, Ticket.Status.ON_HOLD);
        solvedTickets = ticketService.findByStatus(Ticket.Status.SOLVED);
        return "ticket.xhtml?faces-redirect=true";
    }

    public String changeStatus(String statusOut, Ticket ticket) {
        switch (statusOut) {
            case "open":
                ticket.setStatus(Ticket.Status.OPEN);
                break;
            case "onhold":
                ticket.setStatus(Ticket.Status.ON_HOLD);
                break;
            case "solved":
                ticket.setStatus(Ticket.Status.SOLVED);
                break;
        }
        Support support = getLoggedinSupport();
        ticket.setSupport(support);
        ticketService.saveTicket(ticket);
        return supportTicketLists();
    }

    public String getTicketDetails(Ticket ticket) {
        ticketDetails = ticket;
        switch (ticket.getStatus()) {
            case NEW:
                status = "NIEUW";
                break;
            case OPEN:
                status = "OPEN";
                break;
            case ON_HOLD:
                status = "IN WACHT";
                break;
            case SOLVED:
                status = "OPGELOST";
                break;
            case PENDING:
                status = "IN AFWACHTING";
                break;
        }
        return "ticketdetails.xhtml?faces-redirect=true";
    }

    public String sendReply(Ticket ticket) {
        Support support = getLoggedinSupport();
        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
        String date = dt1.format(ticket.getDate().getTime());
        mailService.sendMail(
                ticket.getOrder().getCustomer().getEmail(),
                subject,
                "<p>Beste " + ticket.getOrder().getCustomer().getFirstName() + " " +
                        ticket.getOrder().getCustomer().getLastName() + "</p><p>" + answer +
                        "</p><p>" + support.getFirstName() + " " + support.getLastName() +
                        "<br />Support MediaShop</p><hr /><p>" + date + "</p><p>" + ticket.getDescription() + "</p>"
        );
        return getTicketDetails(ticket);
    }

    public String supportViewSolvedTickets() {
        Support support = getLoggedinSupport();
        solvedTickets = ticketService.findByStatus(Ticket.Status.SOLVED);
        return "ticketarchive.xhtml?faces-redirect=true";
    }

    public CustomerService getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    public EncryptionService getEncryptionService() {
        return encryptionService;
    }

    public void setEncryptionService(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public MailService getMailService() {
        return mailService;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public OrderService getOrderService() {
        return orderService;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TicketService getTicketService() {
        return ticketService;
    }

    public void setTicketService(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Ticket> getNewTickets() {
        return newTickets;
    }

    public void setNewTickets(List<Ticket> newTickets) {
        this.newTickets = newTickets;
    }

    public List<Ticket> getOpenTickets() {
        return openTickets;
    }

    public void setOpenTickets(List<Ticket> openTickets) {
        this.openTickets = openTickets;
    }

    public List<Ticket> getOnholdTickets() {
        return onholdTickets;
    }

    public void setOnholdTickets(List<Ticket> onholdTickets) {
        this.onholdTickets = onholdTickets;
    }

    public List<Ticket> getSolvedTickets() {
        return solvedTickets;
    }

    public void setSolvedTickets(List<Ticket> solvedTickets) {
        this.solvedTickets = solvedTickets;
    }

    public SupportService getSupportService() {
        return supportService;
    }

    public void setSupportService(SupportService supportService) {
        this.supportService = supportService;
    }

    public Ticket getTicketDetails() {
        return ticketDetails;
    }

    public void setTicketDetails(Ticket ticketDetails) {
        this.ticketDetails = ticketDetails;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
