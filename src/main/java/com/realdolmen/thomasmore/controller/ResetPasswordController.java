package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Customer;
import com.realdolmen.thomasmore.service.CustomerService;
import com.realdolmen.thomasmore.service.EncryptionService;
import com.realdolmen.thomasmore.service.MailService;
import com.realdolmen.thomasmore.service.UserService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class ResetPasswordController {

    public ResetPasswordController() {
    }

    @ManagedProperty("#{customerService}")
    private CustomerService customerService;

    @ManagedProperty("#{userService}")
    private UserService userService;

    @ManagedProperty("#{encryptionService}")
    private EncryptionService encryptionService;

    @ManagedProperty("#{mailService}")
    private MailService mailService;

    private String userEmail;
    private String userPassword1;
    private String userPassword2;
    private String message;



    public String sendMail()throws Exception{
        Customer checkCustomer = new Customer();
        checkCustomer.setEmail(userEmail);
        if(!this.userService.checkIfUserEmailExists(checkCustomer)){
            setMessage("Er is geen account met het opgegeven email adres.");
            return "resetpasswordsendmail.xhtml";
        }else{
            Customer customer = customerService.findByEmail(userEmail);
            long id = customer.getId();
            String encrypted = encryptionService.encrypt(id + "");
            mailService.sendMail(
                    userEmail,
                    "Wachtwoord vergeten",
                    "<p>klik <a href=http://localhost:8080/thomas-more-starter-1.0-SNAPSHOT/resetpassword.xhtml?token=" + encrypted + ">hier</a> om uw wachtwoord te veranderen.</p>");
            setMessage("Er is een mail verzonden om uw wachtwoord aan te passen.");
            return "resetpasswordconfirmationpage.xhtml";
        }

}

    public String resetPassword() throws Exception{
        String encrypted = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("token");
        long id = Long.parseLong(encryptionService.decrypt(encrypted));
        Customer customer = customerService.findById(id);
        customer.setPassword(userPassword1);
        customerService.updatePassword(customer);
        setMessage("Uw wachtwoord is aangepast.");
        return "resetpasswordconfirmationpage.xhtml";
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPassword1() {
        return userPassword1;
    }

    public void setUserPassword1(String userPassword1) {
        this.userPassword1 = userPassword1;
    }

    public String getUserPassword2() {
        return userPassword2;
    }

    public void setUserPassword2(String userPassword2) {
        this.userPassword2 = userPassword2;
    }

    public CustomerService getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public EncryptionService getEncryptionService() {
        return encryptionService;
    }

    public void setEncryptionService(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    public MailService getMailService() {
        return mailService;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
