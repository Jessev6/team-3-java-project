package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Cd;
import com.realdolmen.thomasmore.domain.Dvd;
import com.realdolmen.thomasmore.domain.Product;
import com.realdolmen.thomasmore.service.CdService;
import com.realdolmen.thomasmore.service.DvdService;
import com.realdolmen.thomasmore.service.ProductService;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.util.List;

@ManagedBean
@SessionScoped
public class ProductController {
    public Product product;
    private String newName;
    private int newCode;
    private int newPrice;
    private String newDescription;
    private int newStock;
    private List<Product> products;
    public List<Dvd> dvdLijst;
    public List<Cd> cdLijst;
    private int max;


    //Autowired kunnen we niet gebruiken in JSF, daarom gebruiken we hier dit om een spring bean te injecteren.
    @ManagedProperty("#{productService}")
    private ProductService productService;
    @ManagedProperty("#{cdService}")
    private CdService cdService;
    @ManagedProperty("#{dvdService}")
    private DvdService dvdService;

    public List<Product> getProducts() {
        // we gaan nakijken of producten leeg is
        if (products == null) {
            //zoja dan mag je het gewoon met alle producten opvullen
            return products = productService.findAllProducts();
        } else {
            //zo nee, wil dit zeggen dat products binnen een andere functie opgevuld is en moet je deze producten gebruiken.
            return products;
        }

    }


    //getters en setters


    public void setCdService(CdService cdService) {
        this.cdService = cdService;
    }

    public void setDvdService(DvdService dvdService) {
        this.dvdService = dvdService;
    }

    public List<Dvd> getDvdLijst() {
        return dvdLijst;
    }

    public void setDvdLijst(List<Dvd> dvdLijst) {
        this.dvdLijst = dvdLijst;
    }

    public List<Cd> getCdLijst() {
        return cdLijst;
    }

    public void setCdLijst(List<Cd> cdLijst) {
        this.cdLijst = cdLijst;
    }


    public int getNewCode() {
        return newCode;
    }

    public void setNewCode(int newCode) {
        this.newCode = newCode;
    }

    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }

    public int getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(int newPrice) {
        this.newPrice = newPrice;
    }

    public String getNewDescription() {
        return newDescription;
    }

    public void setNewDescription(String newDescription) {
        this.newDescription = newDescription;
    }

    public int getNewStock() {

        return newStock;
    }

    public void setNewStock(int newStock) {
        this.newStock = newStock;
    }

    public Product getProductById(Long id) {
        product = productService.findById(id);
        return product;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * Deze setter MOET aanwezig zijn, anders kan spring deze service niet injecteren.
     */
    public void setProductService(ProductService productService) {
        this.productService = productService;

    }

    public String toDetailPage(Long id) {
        product = productService.findProductById(id);
        return "productdetail?faces-redirect=true";
    }

    public List<Cd> getAllCd() {
        cdLijst = cdService.findAllCd();
        return cdLijst;
    }

    public String getAllCdList() {
        List<Product> resultaten = productService.findAllProducts();
        products.clear();
        for (Product product : resultaten) {
            if (product instanceof Cd) {

                products.add(product);
            }
        }
        return "redirect:/index.xhtml";
    }

    public String getAllDvdList() {
        List<Product> resultaten = productService.findAllProducts();
        products.clear();
        for (Product product : resultaten) {
            if (product instanceof Dvd) {
                products.add(product);
            }
        }
        return "redirect:/index.xhtml";
    }

    public List<Dvd> getAllDvd() {
        dvdLijst = dvdService.findAlldvd();

        return dvdLijst;
    }

    public String laagstePrijs() {
        products = productService.findAllByOrderByPriceAsc();
        return "redirect:/index.xhtml";
    }

    public String orderByName() {
        products = productService.findAllByOrderByNameAsc();
        return "redirect:/index.xhtml";
    }
    public String findAllByStock()
    {
        products = productService.findByStock(0);

        return "redirect:/index.xhtml";
    }
}