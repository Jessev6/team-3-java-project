package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Product;
import com.realdolmen.thomasmore.domain.ShoppingCart;
import com.realdolmen.thomasmore.domain.ShoppingItem;
import com.realdolmen.thomasmore.service.ImageService;
import javassist.NotFoundException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;

@ManagedBean
@SessionScoped
public class CartController {
    ShoppingCart shoppingCart;

    ArrayList<ShoppingItem> items;

    boolean isEmtpy;

    public boolean getIsEmpty() {
        this.setShoppingCart(new ShoppingCart());
        return (shoppingCart.getItemCount() == 0);
    }

    public ArrayList<ShoppingItem> getItems() {
        this.setShoppingCart(new ShoppingCart());
        return this.shoppingCart.getItems();
    }

    public ShoppingCart getShoppingCart() {
        this.setShoppingCart(new ShoppingCart());
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        if(this.shoppingCart == null)
            this.shoppingCart = shoppingCart;
    }

    public void AddProduct(Product product, int amount){
        shoppingCart.addProduct(product, amount);
    }

    public void RemoveProduct(Product product){
        shoppingCart.removeProduct(product);
    }

    public void ChangeAmount(Product product, int amount){
        try {
            shoppingCart.changeProductAmount(product, amount);
        } catch (NotFoundException nfe) {
            System.out.println(nfe.getMessage());
        }
    }
}
