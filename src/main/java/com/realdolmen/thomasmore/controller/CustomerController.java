package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Address;
import com.realdolmen.thomasmore.domain.Customer;
import com.realdolmen.thomasmore.security.UserSession;
import com.realdolmen.thomasmore.service.CustomerService;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped
public class CustomerController {
    private Customer customer;
    private String newPassword;
    private String newPassword2;

    @ManagedProperty("#{userSession}")
    private UserSession userSession;

    @ManagedProperty("#{customerService}")
    private CustomerService customerService;

    @PostConstruct
    public void init(){
        customer = customerService.findById(userSession.getUser().getId());
        if (customer.getBillingAddress() == null) customer.setBillingAddress(new Address());
        if (customer.getDeliveryAddress1() == null) customer.setDeliveryAddress1(new Address());
        if (customer.getDeliveryAddress2() == null) customer.setDeliveryAddress2(new Address());
        newPassword = "";
        newPassword2 = "";
    }


    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void saveCustomer(){
        customerService.saveCustomer(customer);
        userSession.setUser(customer);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Profiel geüpdatet") );
    }


    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPassword2() {
        return newPassword2;
    }

    public void setNewPassword2(String newPassword2) {
        this.newPassword2 = newPassword2;
    }

    public void updateCustomerPassword(){
        customer.setPassword(newPassword);
        customerService.updatePassword(customer);
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Wachtwoord geüpdatet") );
        newPassword = "";
        newPassword2 = "";
    }


    public void setUserSession(UserSession userSession) {
        this.userSession = userSession;
    }

    public CustomerService getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }
}
