package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.*;
import com.realdolmen.thomasmore.service.*;
import org.primefaces.model.UploadedFile;
import org.springframework.util.FileCopyUtils;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;
import java.io.*;
import java.util.List;

@ManagedBean(name="adminController")
@SessionScoped
public class adminController<P extends Product, U extends User> {

    @ManagedProperty("#{cdService}")
    private CdService cdService;

    @ManagedProperty("#{dvdService}")
    private DvdService dvdService;

    @ManagedProperty(("#{supportService}"))
    private SupportService supportService;

    @ManagedProperty("#{adminService}")
    private AdminService adminService;

    @ManagedProperty("#{userService}")
    private UserService userService;

    @ManagedProperty("#{imageService}")
    private ImageService imageService;

    private P newProduct;
    private U newUser;
    private Boolean isEditting;
    private Support newSupport = new Support();
    private UploadedFile file;
    private String errorMessage = "";

    /*
        generic product crud functions
    */

    public List<P> getProducts(String productName) {
        if(productName.equalsIgnoreCase("dvd")){
            return (List<P>) this.dvdService.findAlldvd();
        }

        if(productName.equalsIgnoreCase("cd")){
            return (List<P>) this.cdService.findAllCd();
        }

        return null;
    }

    public String editProduct(Product product){
        this.isEditting = true;
        this.newProduct = (P) product;

        if(product instanceof Cd){
            return "cdForm.xhtml";
        }

        if(product instanceof Dvd){
            return "dvdForm.xhtml";
        }

        return "index.xhtml";
    }

    public String saveProduct() throws IOException {
        if(this.file != null){
            Image image = new Image();
            image.setProduct(newProduct);
            newProduct.setImage(image);
            byte[] content = this.file.getContents();
            if(!this.imageService.uploadImage(content, image))
                throw new IOException("Can't upload to ImageServer");

        }

        if(this.newProduct instanceof Cd){
            this.cdService.saveCd((Cd) this.newProduct);
            return "cd.xhtml";
        }

        if(this.newProduct instanceof Dvd){
            this.dvdService.saveDvd((Dvd) this.newProduct);
            return "dvd.xhtml";
        }

        return "index.xhtml";
    }

    public String deleteProduct(){
        if(this.newProduct instanceof Cd){
            this.cdService.deleteCd((Cd) this.newProduct);
            return "cd.xhtml";
        }

        if(this.newProduct instanceof Dvd){
            this.dvdService.deletedvd((Dvd) this.newProduct);
            return "dvd.xhtml";
        }

        return "index.xhtml";
    }

    public String newProduct(String productName){
        this.isEditting = false;
        if(productName.equalsIgnoreCase("dvd")){
            this.newProduct = (P) new Dvd();
            return "dvdForm.xhtml";
        }

        if(productName.equalsIgnoreCase("cd")){
            this.newProduct = (P) new Cd();
            return "cdForm.xhtml";
        }

        return "index.xhtml";
    }

    /*
        generic user functions
     */

    public List<U> getAllUsers(String type){
        if(type.equalsIgnoreCase("support")){
            return (List<U>) this.supportService.findAllSupportEmployees();
        }

        if(type.equalsIgnoreCase("admin")){
            return (List<U>) adminService.findAllAdmins();
        }

        return null;
    }

    public String editUser(User user){
        this.isEditting = true;
        this.newUser = (U) user;
        return "userForm.xhtml";

    }

    public String saveUser(){
        //check if user email already exists
        if(this.userService.checkIfUserEmailExists(this.newUser) && !isEditting){
            errorMessage = "Er bestaat reeds een gebruiker met dit e-mailadres";
            return "";
        }

        if(isEditting && newUser.getPassword() == ""){
            newUser.setPassword(userService.findByEmail(newUser.getEmail()).getPassword());
        }

        this.errorMessage = "";

        if(this.newUser instanceof Support){
            this.supportService.saveSupport((Support) this.newUser);
            return "support.xhtml";
        }

        if(this.newUser instanceof Admin){
            this.adminService.saveAdmin((Admin) this.newUser);
            return "admin.xhtml";
        }

        return "index.xhtml";
    }

    public String newUser(String type){
        this.isEditting = false;
        if(type.equalsIgnoreCase("support")){
            this.newUser = (U) new Support();
        }

        if(type.equalsIgnoreCase("admin")){
            this.newUser = (U) new Admin();
        }

        return "userForm.xhtml";
    }

    public String deleteUser(){
        if(this.newUser instanceof Support){
            this.supportService.deleteSupport((Support) this.newUser);
            return "support.xhtml";
        }

        if(this.newUser instanceof Admin){
            this.adminService.deleteAdmin((Admin) this.newUser);
            return "admin.xhtml";
        }

        return "index.xhtml";
    }

    public String backToWebshop(){
        this.isEditting = false;
        return "/index?faces-redirect=true";
    }


    //getters and setters
    public void setCdService(CdService cdService) {
        this.cdService = cdService;
    }

    public void setDvdService(DvdService dvdService) {
        this.dvdService = dvdService;
    }

    public void setSupportService(SupportService supportService) {
        this.supportService = supportService;
    }

    public void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setImageService(ImageService imageService) {
        this.imageService = imageService;
    }

    public U getNewUser() {
        return newUser;
    }

    public void setNewUser(U newUser) {
        this.newUser = newUser;
    }

    public Product getNewProduct() {
        return newProduct;
    }

    public void setNewProduct(P newProduct) {
        this.newProduct = newProduct;
    }

    public Boolean getEditting() {
        return isEditting;
    }

    public void setEditting(Boolean editting) {
        isEditting = editting;
    }

    public Support getNewSupport() {
        return newSupport;
    }

    public void setNewSupport(Support newSupport) {
        this.newSupport = newSupport;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
