package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.*;
import com.realdolmen.thomasmore.service.*;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by JUZAU33 on 28/09/2017.
 */
@ManagedBean
@RequestScoped
public class TestController {

    @ManagedProperty("#{adminService}")
    private AdminService adminService;

    @ManagedProperty("#{customerService}")
    private CustomerService customerService;

    @ManagedProperty("#{supportService}")
    private SupportService supportService;

    @ManagedProperty("#{visacardService}")
    private VisaCardService visaCardService;

    @ManagedProperty("#{productService}")
    private ProductService productService;

    public String getHelloWorld() {
        return "Hello, world!";
    }

    public void makeObjects() {

        //admin aanmaken en toevoegen
        Admin admin = new Admin("arne.van.bael@gmail.com", "Arne", "Van Bael", "test");
        adminService.saveAdmin(admin);

        //support medewerker aanmaken en toevoegen
        Support support = new Support("jesse@gmail.com", "Jesse", "Verbruggen", "test");
        supportService.saveSupport(support);

        //customer object aanmaken
        Customer customer = new Customer("Order@gmail.com", "Michiel", "Crabbé", "test");
        //visacard object aanmaken
        VisaCard visaCard = new VisaCard("Michiel", "Crabbé", "000000", "002");
        VisaCard visaCard1 = new VisaCard("Jesse", "Viskens", "000000", "004");

        //visacard aan customer linken
        List<PaymentMethod> listPaymentMethods = customer.getPaymentMethods();
        if (listPaymentMethods == null) listPaymentMethods = new ArrayList<PaymentMethod>();
        listPaymentMethods.add(visaCard);
        customer.setPaymentMethods(listPaymentMethods);


        Product product = new Cd("000", "concrete and gold", 10, "cd foo fighters",2, "arne",
                "foo fighters", new Date(), "rock");
        productService.saveProduct(product);

        Date today = new Date();
        Product product1 = new Cd("001", "Rammstein Live aus Berlin", 33.89, "Rammstein Live tournee cd",456, "Jesse",
                "Ramstein", new Date(), "Hardrock");
        productService.saveProduct(product1);

        Product product2 = new Cd("002", "felix palace", 10, "cd felix palace",9, "arne",
                "felix palace", today, "rock");
        productService.saveProduct(product2);

        Product product3 = new Cd("003", "Bring me the horizon", 10, "cd Bring me the horizon",1, "arne",
                "Bring me the horizon", today, "rock");
        productService.saveProduct(product3);

        Product product4 = new Cd("004", "coldplay",22.55, "Head full of dreams cd met de beste tracks van het moment", 22,"Jesse", "Coldplay, Chris Martin",today, "Poprock");
        productService.saveProduct(product4);

        Product product5 = new Cd("005", "Highway to Hell",15.50, "Highway to Hell is het zesde studioalbum van de Australische hardrockband AC/DC, uitgebracht op 3 augustus 1979", 22,"Jesse", "AC/DC",today, "rock");
        productService.saveProduct(product5);

        Product product6 = new Cd("006", "Ushuaia",17.00, "De nieuwste van k3", 22,"Jesse", "K3",today, "pop");
        productService.saveProduct(product6);





        Order order = new Order(today, today, OrderStatus.ORDERED, customer);
        OrderLine orderLine = new OrderLine(product.getPrice(), 1, product, order);
        OrderLine orderline2 = new OrderLine(product2.getPrice(), 2, product2, order);
        OrderLine orderline3 = new OrderLine(product3.getPrice(), 3, product3, order);


        List<OrderLine> orderlines = order.getOrderLines();
        if (orderlines == null) orderlines = new ArrayList<OrderLine>();
        orderlines.add(orderLine);
        orderlines.add(orderline2);
        orderlines.add((orderline3));
        order.setOrderLines(orderlines);

        List<Order> orderList = customer.getOrders();
        if (orderList == null) orderList = new ArrayList<Order>();
        orderList.add(order);

        customer.setOrders(orderList);

        //customer wordt opgeslagen (--> paymenthmethod wordt ook aangemaakt door cascade persist)
        customerService.saveCustomer(customer);
    }

    public void makeProducts() {
        Date today = new Date();

        Product product = new Cd("000", "concrete and gold", 10, "cd foo fighters",2, "arne",
                "foo fighters", new Date(), "rock");
        productService.saveProduct(product);

        Product product1 = new Cd("001", "Rammstein Live aus Berlin", 33.89, "Rammstein Live tournee cd",456, "Jesse",
                "Ramstein", today, "Hardrock");
        productService.saveProduct(product1);

        Product product2 = new Cd("002", "felix palace", 10, "cd felix palace",9, "arne",
                "felix palace", today, "rock");
        productService.saveProduct(product2);

        Product product3 = new Cd("003", "Bring me the horizon", 10, "cd Bring me the horizon",1, "arne",
                "Bring me the horizon", today, "rock");
        productService.saveProduct(product3);

        Product product4 = new Cd("004", "Head full of dreams",22.55, "Head full of dreams cd met de beste tracks van het moment", 22,"Jesse", "Coldplay, Chris Martin",today, "Poprock");
        productService.saveProduct(product4);

        Product product5 = new Cd("005", "Highway to Hell",15.50, "Highway to Hell is het zesde studioalbum van de Australische hardrockband AC/DC, uitgebracht op 3 augustus 1979", 22,"Jesse", "AC/DC",today, "rock");
        productService.saveProduct(product5);

        Product product6 = new Cd("006", "Ushuaia",17.00, "De nieuwste van k3", 22,"Jesse", "K3",today, "pop");
        productService.saveProduct(product6);
/*dvd's*/
        Product product7 = new Dvd("007", "Mr Bean's holiday", 14.52, "Mr. Bean's Holiday is de tweede film over het personage Mr. Bean, na Bean: The Ultimate Disaster Movie, en werd uitgebracht in 2007. Volgens de acteur, Rowan Atkinson, is dit het laatste avontuur dat Bean zal beleven. ",15, "Steve Bendelack", "ikke", today, "weetikni", "Rowan Adkinson, Emma de Caunes, Max Baldry", 90, "English", "English", "Humor");
        productService.saveProduct(product7);

        Product product8 = new Dvd("008", "Saving private Ryan", 33.55, "A movie about a risky rescue mission during the normandy landings",7, "Steven Spielberg", "ikke", today, "weetikni", "Jesse Viskens, Vin Diezel", 123, "English", "English", "War, action");
        productService.saveProduct(product8);

        Product product9 = new Dvd("009", "The Fate of the Furious", 43.55, "The Fate of the Furious, ook bekend als Fast & Furious 8, is een Amerikaanse actiefilm uit 2017. ",72, "F. Gary Gray", "Jesse", today, "test", "Vin Diesel, Dwayne Johnson, Jason Statham, Michelle Rodríguez", 138, "English", "English", "action");
        productService.saveProduct(product9);

        Product product10 = new Dvd("010", "22 Jump Street", 43.55, "22 Jump Street is een Amerikaanse komedie uit 2014 geregisseerd door Phil Lord en Christopher Miller en geproduceerd door Jonah Hill en Channing Tatum die ook de hoofdrollen op zich namen.  ",2, "Phil Lord, Chris Miller", "Jesse", today, "test", "Channing Tatum, Jonah Hill, Ice cube, Amber Stevens ", 112, "English", "English", "action");
        productService.saveProduct(product10);

        Product product11 = new Dvd("011", "Titanic", 12.55, "Titanic is een film uit 1997 van filmregisseur James Cameron, geproduceerd door 20th Century Fox, Paramount Pictures en Spyglass Entertainment.  ",81, "James Cameron", "Jesse", today, "test", "Leonardo DiCaprio, Kate Winslet ", 195, "English", "English", "action");
        productService.saveProduct(product11);

        Product product12 = new Dvd("012", "The Dark Knight", 22.55, "The Dark Knight is een Amerikaanse film van regisseur Christopher Nolan, gebaseerd op het personage Batman. De film is het vervolg op Batman Begins uit 2005. De titel van de film betekent De Donkere Ridder, een bijnaam van Batman. ",31, "Christopher Nolan", "Jesse", today, "test", "Christian Bale Morgan Freeman ", 152, "English", "English", "action");
        productService.saveProduct(product12);


    }

    public void extraOrder() {

    }

    //setters nodig voor injectie door spring
    public void setAdminService(AdminService adminService) {


        this.adminService = adminService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    public void setSupportService(SupportService supportService) {
        this.supportService = supportService;
    }

    public void setVisaCardService(VisaCardService visaCardService) {
        this.visaCardService = visaCardService;
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }
}