package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Order;
import com.realdolmen.thomasmore.domain.User;
import com.realdolmen.thomasmore.security.UserSession;
import com.realdolmen.thomasmore.service.OrderService;
import com.realdolmen.thomasmore.service.PaymentService;
import com.realdolmen.payment.jaxb.PaymentRequest;
import com.realdolmen.payment.jaxb.PaymentResponse;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

@ManagedBean(name="paymentController")
@ViewScoped
public class paymentController {
    @ManagedProperty("#{paymentService}")
    private PaymentService paymentService;

    @ManagedProperty("#{orderService}")
    private OrderService orderService;

    @ManagedProperty("#{userSession}")
    private UserSession userSession;

    public String testPayment() {
        PaymentRequest newRequest = new PaymentRequest();
        newRequest.setMerchantId("1");
        newRequest.setCreditCardNumber("4111111111111111");
        newRequest.setCreditCardHolderName("Van Bael Arne");
        newRequest.setCreditCardExpirationDate("4/19");
        newRequest.setCvcCode("111");
        newRequest.setAmount(100);
        PaymentResponse response = new PaymentResponse();
        try{
             response = paymentService.payment(newRequest);
        } catch (Exception ex){
            return "/customer/betalingmislukt.xhtml?faces-redirect=true";
        }
        if(response.isSuccess())
            return "successful";

        return "/customer/betalingmislukt.xhtml?faces-redirect=true";
    }

    public String createPayment(Order order){
        String paymentStatus = testPayment();
        if(paymentStatus == "successful"){
            orderService.saveOrder(order);
            return "/customer/redirectorders.xhtml?faces-redirect=true";
        }
        return paymentStatus;
    }

    public void setPaymentService(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    public void setUserSession(UserSession userSession) {
        this.userSession = userSession;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

}
