package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Product;
import com.realdolmen.thomasmore.domain.Review;
import com.realdolmen.thomasmore.domain.User;
import com.realdolmen.thomasmore.service.ReviewService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import java.util.Date;
import java.util.List;

@ManagedBean
@ViewScoped
public class ReviewController {
    Review review;
    String comment;
    int rating;

    @ManagedProperty("#{reviewService}")
    private ReviewService reviewService;

    @ManagedProperty("#{userSession.user}")
    private User user;

    @ManagedProperty("#{productController.product}")
    private Product product;

    @PostConstruct
    public void init(){
        if (user!= null){
            this.review = reviewService.findByProductAndUser(product, user);
            if (this.review==null){
                this.review = new Review();
            }

            review.setUser(user);
            review.setProduct(product);

            comment = review.getComment();
            rating = review.getRating();
        }

    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public List<Review> getReviews() {
        return reviewService.findAllByProduct(product);
    }

    public Review getReview(){
        return this.review;
    }

    public void setReview(Review review) {
        this.review = review;
    }


    public void saveReview(){
        review.setDate(new Date());
        review.setComment(comment);
        review.setRating(rating);
        reviewService.saveReview(review);
    }

    public int getReviewCount(){
        return reviewService.countAllByProduct(product);
    }

    public int getRatingCount(int rating){
        return reviewService.countAllByProductAndRating(product,rating);
    }


    public void setReviewService(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}