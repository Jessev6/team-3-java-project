package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Customer;
import com.realdolmen.thomasmore.domain.Order;
import com.realdolmen.thomasmore.domain.OrderLine;
import com.realdolmen.thomasmore.domain.OrderStatus;
import com.realdolmen.thomasmore.service.CdService;
import com.realdolmen.thomasmore.service.CustomerService;
import com.realdolmen.thomasmore.service.OrderLineService;
import com.realdolmen.thomasmore.service.OrderService;
import org.primefaces.context.RequestContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@SessionScoped
public class orderController {
    @ManagedProperty("#{orderService}")
    private OrderService orderService;

    @ManagedProperty("#{orderLineService}")
    private OrderLineService orderLineService;

    @ManagedProperty("#{customerService}")
    private CustomerService customerService;

    private List<Order> orders = new ArrayList<>();
    private String email;
    private Order order;
    private OrderLine tempDelete;
    private List<OrderLine> orderLinesToDelete;

    private List<Order> ordersHistory = new ArrayList<>();
    private List<Order> ordersOpen = new ArrayList<>();

    private void getAllOrders(){
        orders = orderService.findAllOrders();
    }

    public void getOrdersByCustomer(){
        if(this.email.trim().length() > 0) {
            this.orders = orderService.getOrdersByCustomersEmail(email);
        }
        else{
            email=null;
        }
    }

    public OrderStatus[] getOrderStatuses(){
        return OrderStatus.values();
    }

    public String editOrder(Order order){
        this.orderLinesToDelete = new ArrayList<>();
        this.order = order;
        return "orderForm.xhtml";
    }

    public String deleteOrder(){
        orderLinesToDelete = orderLineService.findOrderLinesByOrders(this.order);
        this.order.getOrderLines().clear();
        deleteOrderLines();
        Order order = orderService.get(this.order.getId());
        orderService.deleteOrder(order);
        orders = orderService.findAllOrders();
        this.order = null;
        return "orders.xhtml";
    }

    private void deleteOrderLines() {
        //set fk in orderline to null
        for(OrderLine orderline : orderLinesToDelete) {
            orderline.setOrder(null);
            orderLineService.saveOrderLine(orderline);
        }
        orderService.saveOrder(order);

        //delete orphan orderlines
        for(OrderLine orderline : orderLinesToDelete) {
            orderLineService.deleteOrderLineById(orderline.getId());
        }
    }

    public String saveOrder(){
        if(orderLinesToDelete.size() > 0) {
            deleteOrderLines();
            if(order.getOrderLines().size() == 0) {
                Order order = orderService.get(this.order.getId());
                orderService.deleteOrder(order);
            }
        }
        else{
            orderService.saveOrder(order);
        }

        tempDelete = null;
        order = null;
        orderLinesToDelete = new ArrayList<>();
        return "orders.xhtml";
    }

    public void deleteOrderLine(){
        orderLinesToDelete.add(tempDelete);
        this.order.getOrderLines().remove(tempDelete);
    }

    //Customer view their orders
    public Customer getLoggedinCustomer() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = ((UserDetails) principal).getUsername();
        Customer customer = customerService.findByEmail(email);
        return customer;
    }
    public String customerOrders(){
        Customer customer = getLoggedinCustomer();
        ordersHistory = orderService.findAllHistoryOrdersByCustomer(customer);
        for (Order orderHistory: ordersHistory) {
            double price = 0;
            for (OrderLine orderLine: orderHistory.getOrderLines()){
                price += (orderLine.getActualPrice() * orderLine.getAmount());
            }
            orderHistory.setPrice(price);
        }
        ordersOpen = orderService.findAllFutureOrdersByCustomer(customer);
        for (Order orderOpen: ordersOpen) {
            double price = 0;
            for (OrderLine orderLine: orderOpen.getOrderLines()){
                price += (orderLine.getActualPrice() * orderLine.getAmount());
            }
            orderOpen.setPrice(price);
        }
        return "/customer/orders.xhtml";
    }



    //getters and setters
    public List<Order> getOrders() {
        if(email==null) {
            getAllOrders();
        }
        return orders;
    }

    public List<OrderLine> getOrderLinesToDelete() {
        return orderLinesToDelete;
    }

    public void setOrderLinesToDelete(List<OrderLine> orderLinesToDelete) {
        this.orderLinesToDelete = orderLinesToDelete;
    }

    public OrderLine getTempDelete() {
        return tempDelete;
    }

    public void setTempDelete(OrderLine tempDelete) {
        this.tempDelete = tempDelete;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Order getOrder() {
        return order;
    }
    public void setOrder(Order order) {
        this.order = order;
    }

    public List<Order> getOrdersHistory() {
        return ordersHistory;
    }

    public void setOrdersHistory(List<Order> ordersHistory) {
        ordersHistory = ordersHistory;
    }

    public List<Order> getOrdersOpen() {
        return ordersOpen;
    }

    public void setOrdersOpen(List<Order> ordersOpen) {
        ordersOpen = ordersOpen;
    }

    //setters for DI
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    public void setOrderLineService(OrderLineService orderLineService) {
        this.orderLineService = orderLineService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }
}
