package com.realdolmen.thomasmore.controller;

import com.realdolmen.thomasmore.domain.Admin;
import com.realdolmen.thomasmore.domain.Customer;
import com.realdolmen.thomasmore.domain.Support;
import com.realdolmen.thomasmore.security.UserSession;
import com.realdolmen.thomasmore.service.CdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;


@ManagedBean(name = "authController")
@SessionScoped
public class authController {

    @ManagedProperty("#{userSession}")
    private UserSession userSession;


    public boolean userLoggedIn(){
        if(userSession.getUser() != null){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean canAccessCustomer() {
        return userLoggedIn() && (userSession.getUser() instanceof Customer);
    }

    public boolean canAccessSupport() {
        return userLoggedIn() && (userSession.getUser() instanceof Support || userSession.getUser() instanceof Admin);
    }

    public boolean canAccessAdmin() {
        return userLoggedIn() && userSession.getUser() instanceof Admin;
    }

    public void setUserSession(UserSession userSession) {
        this.userSession = userSession;
    }
}
