package com.realdolmen.thomasmore.security;

import com.realdolmen.thomasmore.config.SecurityConfiguration;
import com.realdolmen.thomasmore.domain.Admin;
import com.realdolmen.thomasmore.domain.Customer;
import com.realdolmen.thomasmore.domain.Support;
import com.realdolmen.thomasmore.domain.User;
import com.realdolmen.thomasmore.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service("customUserDetailsService")
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserSession userSession;

    private Collection<GrantedAuthority> getGrantedAuthorities(User user){
        Collection<GrantedAuthority> authorityCollection = new ArrayList<>();

        if(user instanceof Admin){
            authorityCollection.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return SecurityConfiguration.Admin;
                }
            });

            authorityCollection.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return SecurityConfiguration.Support;
                }
            });
        }

        if(user instanceof Customer){
            authorityCollection.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return SecurityConfiguration.Customer;
                }
            });
        }

        if( user instanceof Support){
            authorityCollection.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return SecurityConfiguration.Support;
                }
            });
        }



        return authorityCollection;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User foundUser = userRepository.findByEmail(email);

        if(foundUser==null){
            //FacesContext.getCurrentInstance().addMessage("loginForm:errorMessage", new FacesMessage("Dit e-mailadres werd niet gevonden"));
            System.out.println("User not found");
            throw new UsernameNotFoundException("Username not found");
        }

        userSession.setUser(foundUser);

        return new org.springframework.security.core.userdetails.User(foundUser.getEmail(), foundUser.getPassword(),
                true, true, true, true, getGrantedAuthorities(foundUser));
    }
}