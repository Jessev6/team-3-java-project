package com.realdolmen.thomasmore.security;

import com.realdolmen.thomasmore.config.SecurityConfiguration;
import com.realdolmen.thomasmore.domain.Admin;
import com.realdolmen.thomasmore.domain.Customer;
import com.realdolmen.thomasmore.domain.Support;
import com.realdolmen.thomasmore.domain.User;
import com.realdolmen.thomasmore.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by JUZAU33 on 8/08/2017.
 */

@Service
public class DatabaseSecurityProvider implements AuthenticationProvider {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserSession userSession;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        final User user = userRepository.findByEmailAndPassword(username, password);

        if (user == null) {
            throw new BadCredentialsException("Invalid username or password!");
        }

        Collection<GrantedAuthority> authorityCollection = new ArrayList<>();
        if(user instanceof Admin){
            authorityCollection.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return SecurityConfiguration.Admin;
                }
            });

            authorityCollection.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return SecurityConfiguration.Support;
                }
            });
        }

        if(user instanceof Customer){
            authorityCollection.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return SecurityConfiguration.Customer;
                }
            });
        }

        if( user instanceof Support){
            authorityCollection.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return SecurityConfiguration.Support;
                }
            });
        }

        userSession.setUser(user);

        return new UsernamePasswordAuthenticationToken(authentication.getName(), authentication.getCredentials(), authorityCollection);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
