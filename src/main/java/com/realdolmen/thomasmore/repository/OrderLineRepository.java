package com.realdolmen.thomasmore.repository;


import com.realdolmen.thomasmore.domain.Order;
import com.realdolmen.thomasmore.domain.OrderLine;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderLineRepository extends CrudRepository<OrderLine, Long> {

    OrderLine findById(Long id);
    List<OrderLine> findByOrder(Order order);
    List<OrderLine> findAll();
}
