package com.realdolmen.thomasmore.repository;


import com.realdolmen.thomasmore.domain.Order;
import com.realdolmen.thomasmore.domain.Support;
import com.realdolmen.thomasmore.domain.Ticket;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends CrudRepository<Ticket, Long> {

    Ticket findById(Long id);
    List<Ticket> findBySupport(Support support);
    List<Ticket> findBySupportAndStatus(Support support, Ticket.Status status);
    List<Ticket> findByOrder(Order order);
    List<Ticket> findByOrderAndStatus(Order order, Ticket.Status status);
    List<Ticket> findByStatus(Ticket.Status status);
    List<Ticket> findAll();

}
