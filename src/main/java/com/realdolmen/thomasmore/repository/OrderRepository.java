package com.realdolmen.thomasmore.repository;


import com.realdolmen.thomasmore.domain.Customer;
import com.realdolmen.thomasmore.domain.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

    Order findById(Long id);
    List<Order> findAll();

    List<Order> findAllByCustomerEmail(String email);

    List<Order> findAllByCustomerAndDeliveryDateIsGreaterThanEqual(Customer customer, Date date);

    List<Order> findAllByCustomerAndDeliveryDateIsLessThan(Customer customer, Date date);
}
