package com.realdolmen.thomasmore.repository;
import com.realdolmen.thomasmore.domain.Product;
import com.realdolmen.thomasmore.domain.Review;
import com.realdolmen.thomasmore.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Een interface die CrudRepository extend, we geven het type van de entity mee en de primary key. (Employee en Long)
 */
@Repository
public interface ReviewRepository extends CrudRepository<Review, Long> {
    Review findByRating(int rating);
    Review findByDate(Date date);
    List<Review> findAll();
    List<Review> findAllByProduct(Product product);
    List<Review> findAllByProductAndRating(Product product, int rating);
    int countAllByProduct(Product product);
    int countAllByProductAndRating(Product product, int rating);
    Review findByProductAndUser(Product product, User user);


}
