package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.Cd;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CdRepository extends CrudRepository<Cd, Long> {
    List<Cd>  findAllByOrderByName();
    List<Cd>  findAllByOrderByNameAsc();

}