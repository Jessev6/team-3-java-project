package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.Admin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdminRepository extends CrudRepository<Admin, Long> {

    Admin findByFirstNameAndLastName(String firstName, String lastName);
    List<Admin> findAll();

}
