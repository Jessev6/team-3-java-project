package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    Customer findByFirstNameAndLastName(String firstName, String lastName);
    List<Customer> findAll();

    Customer findById(long id);


    Customer findByEmail(String email);
}
