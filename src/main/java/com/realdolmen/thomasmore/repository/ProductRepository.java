package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
    Product findById(long id);
    Product findProductByCode(String code);
    List<Product> findAll();
    List<Product> findAllByOrderByPriceAsc();
    List<Product> findAllByOrderByNameAsc();
    List<Product> findAllByStockGreaterThanOrderByStockAsc(int aantal);
}
