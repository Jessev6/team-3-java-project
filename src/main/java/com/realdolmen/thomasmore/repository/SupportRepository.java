package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.Support;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupportRepository extends CrudRepository<Support, Long> {

    Support findByFirstNameAndLastName(String firstName, String lastName);
    List<Support> findAll();

    Support findByEmail(String email);

}
