package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.Dvd;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DvdRepository extends CrudRepository<Dvd, Long> {
    List<Dvd> findAllByOrderByName();


    //List<Dvd> findAllByNameOrderByName();
    List<Dvd> findAll();
}
