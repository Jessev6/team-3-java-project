package com.realdolmen.thomasmore.repository;

import com.realdolmen.thomasmore.domain.PaymentMethod;
import com.realdolmen.thomasmore.domain.VisaCard;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VisaCardRepository extends CrudRepository<VisaCard, Long> {

    VisaCard findOne(Long aLong);
    List<VisaCard> findAll();
}
